package com.example.patrick.sms;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnStart;
    EditText varMsg, varPhoneNo;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart= findViewById(R.id.idbtnStart);
        varMsg= findViewById(R.id.idTxtMsg);
        varPhoneNo= findViewById(R.id.idTxtPhoneNo);
    }

    public void sendSms(View v){
        int permissionCheck= ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if(permissionCheck== PackageManager.PERMISSION_GRANTED){
            MyMessage();
        }else{
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        }
    }

    public void MyMessage(){
        String myNumber=varPhoneNo.getText().toString().trim();
        String myMsg=varMsg.getText().toString().trim();

        if(myNumber==null||myNumber.equals("")||myMsg.equals("")){
            Toast.makeText(this,"Campo no puede ser vacio",Toast.LENGTH_SHORT).show();
        }else{
            if(TextUtils.isDigitsOnly(myNumber)){
                SmsManager smsManager= SmsManager.getDefault();
                smsManager.sendTextMessage(myNumber,null,myMsg,null,null);
                Toast.makeText(this,"Mensaje Enviado",Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(this,"Ingrese solo numeros",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[]permissions, int[]grantResults){
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);

        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_SEND_SMS:
            {
                if(grantResults.length>=0&& grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    MyMessage();
                }else{
                    Toast.makeText(this,"No tiene permisos para realizar la accion",Toast.LENGTH_SHORT);
                }
            }
        }
    }
}
