package ec.edu.uce.vista;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import android.util.Log;

import ec.edu.uce.controlador.VehiculosAdapter;
import ec.edu.uce.model.Vehiculo;
import ec.edu.uce.servicios.VehiculoService;
import ec.edu.uce.util.PlacaComparator;
import ec.edu.uce.util.RecyclerTouchListener;


public class ListActivity extends Activity {

    private VehiculoService storage;
    private RecyclerView recView;
    private Button setPref;
    List<Vehiculo> data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storage = new VehiculoService();// cambiar storage
        setContentView(R.layout.activity_list);

        //Inicialización RecyclerView
        recView = findViewById(R.id.RecView);
        recView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recView.setLayoutManager(linearLayoutManager);

        recView.addOnItemTouchListener(
                new RecyclerTouchListener(this, recView,
                        new RecyclerTouchListener.OnTouchActionListener() {
                            @Override
                            public void onClick(View view, int position) {
                                Intent intent = new Intent(ListActivity.this, CreateActivity.class);
                                Bundle b = new Bundle();
                                b.putInt("key", data.get(position).getId()); //Your id
                                intent.putExtras(b); //Put your id to your next Intent
                                startActivity(intent);
                            }

                            @Override
                            public void onRightSwipe(View view, int position) {
                                if (storage.remove(data.get(position))) {
                                    try {
                                        fillData();
                                    } catch (Exception e) {
                                        Toast.makeText(getApplicationContext(), "Vehiculo No Eliminado", Toast.LENGTH_LONG).show();
                                    }
                                    Toast.makeText(getApplicationContext(), "Vehiculo Eliminado", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Vehiculo No Eliminado", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onLeftSwipe(View view, int position) {
                            }
                        }));

        setPref = (Button) findViewById(R.id.SetOrderList);
        setPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this,
                        ConfigActivity.class));
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        fillData();
    }

    private void fillData() {
        data = storage.getData();
        PlacaComparator pc = new PlacaComparator(leerPreferencias());
        Collections.sort(data, pc);
        final VehiculosAdapter adaptador = new VehiculosAdapter(data, this);
        recView.setAdapter(adaptador);
    }

    public void linkCreate(View view) {
        Intent list = new Intent(ListActivity.this, CreateActivity.class);
        startActivity(list);
    }


    /**
     * @return 1 si se ordena ascendente, otro descendente
     */
    public int leerPreferencias() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(
                ListActivity.this);
        Integer opc = Integer.parseInt(settings.getString("orderBox", "0"));
        Log.i("INFO:","La opcion es: "+opc);
        return opc;

    }

}
