package com.example.alex.ej_310_g_01;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class SmsActivity extends Activity {
    //Dirección del dispositivo donde nos gustaría enviar (número de teléfono, código corto, etc.)
    private static final String RECIPIENT_ADDRESS = "0984648251";
    private static final String ACTION_SENT ="com.examples.sms.SENT";
    private static final String ACTION_DELIVERED ="com.examples.sms.DELIVERED";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button sendButton = new Button(this);
        sendButton.setText("Hail the Mothership");

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS("Beam us up!");
            }
        });
        setContentView(sendButton);
    }
    @Override
    protected void onResume() {
        super.onResume();
//Monitorear el estado de las operaciones
        registerReceiver(sent, new IntentFilter(ACTION_SENT));
        registerReceiver(delivered, new IntentFilter(ACTION_DELIVERED));
    }
    @Override
    protected void onPause() {
        super.onPause();
//Asegúrate de que los receptores no estén activos mientras estamos en el segundo plano
        unregisterReceiver(sent);
        unregisterReceiver(delivered);
    }

    private void sendSMS(String message) {
        PendingIntent sIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_SENT), 0);
        PendingIntent dIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_DELIVERED), 0);
//Send the message
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(RECIPIENT_ADDRESS, null, message, sIntent, dIntent);
    }

    private BroadcastReceiver sent = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(SmsActivity.this, "SMS sent", Toast.LENGTH_SHORT).show();
                    break;
//Handle sent success
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(SmsActivity.this, "Generic Failure!", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(SmsActivity.this, "No service!", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(SmsActivity.this, "Null PDU", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(SmsActivity.this, "Radio off!", Toast.LENGTH_SHORT).show();
//Handle sent error
                    break;
            }
        }
    };

    private BroadcastReceiver delivered = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(SmsActivity.this, "SMS delivered", Toast.LENGTH_SHORT).show();
//Handle delivery success
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(SmsActivity.this, "SMS not delivered", Toast.LENGTH_SHORT).show();
//Handle delivery failure
                    break;
            }
        }
    };
}