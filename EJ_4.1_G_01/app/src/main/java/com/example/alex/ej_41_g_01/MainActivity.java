package com.example.alex.ej_41_g_01;

import android.app.Dialog;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SupportErrorDialogFragment;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;


public class MainActivity extends AppCompatActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static final String TAG = "AndroidRecipes";
    private static final int UPDATE_INTERVAL = 15 * 1000;
    private static final int FASTEST_UPDATE_INTERVAL = 2 * 1000;
    /* Interfaz Cliente para Play Services */
    private LocationClient mLocationClient;
    /* Metadatos sobre las actualizaciones que queremos recibir */
    private LocationRequest mLocationRequest;
    /* Última ubicación conocida del dispositivo */
    private Location mCurrentLocation;
    private TextView mLocationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationView = new TextView(this);
        setContentView(mLocationView);
//Verificar Play Services está activo y actualizado
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                Log.d(TAG, "Google Play Services is ready to go!");
                break;
            default:
                showPlayServicesError(resultCode);
                return;
        }
        //Agregar monitoreo de actualizaciones de ubicación
        mLocationClient = new LocationClient(this, this, this);
        mLocationRequest = LocationRequest.create()
                //Establezca el nivel de precisión requerido
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                //Establezca la frecuencia deseada (inexacta) de actualizaciones de ubicación
                .setInterval(UPDATE_INTERVAL)
                //Acelere la tasa máxima de solicitudes de actualización
                .setFastestInterval(FASTEST_UPDATE_INTERVAL);
    }
    @Override
    public void onResume() {
        super.onResume();
        //Cuando pasamos al primer plano, adjunte a Play Services
        mLocationClient.connect();
    }
    @Override
    public void onPause() {
        super.onPause();
        //Deshabilitar actualizaciones cuando no estamos en primer plano
        if (mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
        }
//Desconectarse de Play Services
        mLocationClient.disconnect();
    }

    private void updateDisplay() {
        if(mCurrentLocation == null) {
            mLocationView.setText("Determining Your Location...");
        } else {
            mLocationView.setText(String.format("Your Location:\n%.2f, %.2f",
                    mCurrentLocation.getLatitude(),
                    mCurrentLocation.getLongitude()));
        }
    }
/** Ubicación de Play Services */
/*
* Cuando falta Play Services o en la versión incorrecta,
* la biblioteca del cliente lo ayudará con un diálogo para ayudar al usuario a actualizar.
*/
private void showPlayServicesError(int errorCode) {
// Obtener el cuadro de diálogo de error de Google Play Services
    Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this,
            1000 /* RequestCode */);
// Si Google Play Services puede proporcionar un cuadro de diálogo de error
    if (errorDialog != null) {
// Crear un nuevo DialogFragment para el diálogo de error
        SupportErrorDialogFragment errorFragment = SupportErrorDialogFragment.newInstance(errorDialog);
// Mostrar el cuadro de diálogo de error en DialogFragment
        errorFragment.show(
                getSupportFragmentManager(),
                "Location Updates");
    }
}
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Connected to Play Services");
//Obtenga la última ubicación conocida de inmediato
        mCurrentLocation = mLocationClient.getLastLocation();
//Regístrese para recibir actualizaciones
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }
    @Override
    public void onDisconnected() { }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) { }
    /** Retrollamadas de LocationListener */
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Received location update");
        mCurrentLocation = location;
        updateDisplay();
    }
}