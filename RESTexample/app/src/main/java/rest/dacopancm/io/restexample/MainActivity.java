package rest.dacopancm.io.restexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = findViewById(R.id.button);
        Button buttonSoap = findViewById(R.id.buttonSoap);

        final EditText latIn = findViewById(R.id.latIn);
        final EditText longIn = findViewById(R.id.longIn);

        latIn.setText("-0.180653");
        longIn.setText("-78.467834");

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double lat = -0.180653, lon = -78.467834;
                String units = "imperial";
                String url = String.format("http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&units=%s&appid=%s",
                        latIn.getText().toString(), longIn.getText().toString(), units, "acd56d8951663474caa26f7754f54f86");

                TextView textView = findViewById(R.id.textView);
                new AsynTaskWeatherREST(textView).execute(url);
            }
        });

        buttonSoap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://optativa-api.azurewebsites.net/server.php";

                TextView textView = findViewById(R.id.textView);
                new AsynTaskWeatherSoap(textView).execute(url, latIn.getText().toString(), longIn.getText().toString());
            }
        });

    }

}
