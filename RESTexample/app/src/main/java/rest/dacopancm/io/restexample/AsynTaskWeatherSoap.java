package rest.dacopancm.io.restexample;

import android.os.AsyncTask;
import android.util.Xml;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dacop on 12/12/2017.
 */

public class AsynTaskWeatherSoap extends AsyncTask<String, Void, String> {
    private TextView textView;

    public AsynTaskWeatherSoap(TextView textView) {
        this.textView = textView;
    }

    @Override
    protected String doInBackground(String... strings) {
        String weather = "UNDEFINED";
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            urlConnection.setRequestProperty("SOAPAction",
                    "GetWeather");

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "  <soap:Body>\n" +
                    "    <GetWeather xmlns=\"www.dacopancm.io\">\n" +
                    "      <lat>" + strings[1] + "</lat>\n" +
                    "      <long>" + strings[2] + "</long>\n" +
                    "    </GetWeather>\n" +
                    "  </soap:Body>\n" +
                    "</soap:Envelope>";
            OutputStream reqStream = urlConnection.getOutputStream();
            reqStream.write(request.getBytes());
            reqStream.flush();


            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            weather = builder.toString();

            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new ByteArrayInputStream(weather.getBytes()), null);
            parser.nextTag();
            parser.nextTag();
            parser.nextTag();
            parser.nextTag();
            parser.next();
            weather = parser.getText();

            urlConnection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weather;
    }

    @Override
    protected void onPostExecute(String temp) {
        textView.setText("Current Weather: " + temp);
    }
}

